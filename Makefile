CXXFLAGS= -Ofast -std=c++20 -s -flto

all: c_file.exe c_file_unlocked.exe filebuf_file.exe iobuf_file.exe fstream.exe stdio.exe

c_file.exe:
	$(CXX) -o c_file.exe c_file.cc $(CXXFLAGS) $(EXTRAFLAGS)
c_file_unlocked.exe:
	$(CXX) -o c_file_unlocked.exe c_file_unlocked.cc $(CXXFLAGS) $(EXTRAFLAGS)
filebuf_file.exe:
	$(CXX) -o filebuf_file.exe filebuf_file.cc $(CXXFLAGS) $(EXTRAFLAGS)
iobuf_file.exe:
	$(CXX) -o iobuf_file.exe iobuf_file.cc $(CXXFLAGS)
fstream.exe:
	$(CXX) -o fstream.exe fstream.cc $(CXXFLAGS)
stdio.exe:
	$(CXX) -o stdio.exe stdio.cc $(CXXFLAGS)

clean:
	rm *.exe