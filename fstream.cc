#include<fast_io.h>
#include<fast_io_device.h>
#include<fast_io_driver/timer.h>
#include<fast_io_legacy.h>
#include<vector>

int main()
{
	constexpr std::size_t N(
#ifdef FAST_IO_BENCH_N
FAST_IO_BENCH_N
#else
10000000
#endif
);
	{
	fast_io::timer t(u8"output");
	std::ofstream fout("fstream.txt",std::ofstream::binary);
	for(std::size_t i{};i!=N;++i)
		fout<<i<<'\n';
	}
	std::vector<std::size_t> vec(N);
	{
	fast_io::timer t(u8"input");
	std::ifstream fin("fstream.txt",std::ifstream::binary);
	for(std::size_t i{};i!=N;++i)
		fin>>i;
	}
}
