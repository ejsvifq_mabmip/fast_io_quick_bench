#include<fast_io.h>
#include<fast_io_device.h>
#include<fast_io_driver/timer.h>
#include<fast_io_legacy.h>
#include<vector>

int main()
{
	constexpr std::size_t N(
#ifdef FAST_IO_BENCH_N
FAST_IO_BENCH_N
#else
10000000
#endif
);
	{
	fast_io::timer t(u8"output");
	fast_io::filebuf_file obf(u"iobuf_nt_file.txt",fast_io::open_mode::out);
	for(std::size_t i{};i!=N;++i)
		println(obf,i);
	}
	std::vector<std::size_t> vec(N);
	{
	fast_io::timer t(u8"input");
	fast_io::filebuf_file ibf(u"iobuf_nt_file.txt",fast_io::open_mode::in);
	for(std::size_t i{};i!=N;++i)
		scan(ibf,vec[i]);
	}
}
