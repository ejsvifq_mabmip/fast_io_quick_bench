#include<fast_io.h>
#include<fast_io_device.h>
#include<fast_io_driver/timer.h>
#include<vector>

int main()
{
	constexpr std::size_t N(
#ifdef FAST_IO_BENCH_N
FAST_IO_BENCH_N
#else
10000000
#endif
);
	{
	fast_io::timer t(u8"output");
	fast_io::obuf_file obf(L"iobuf_file.txt");
	for(std::size_t i{};i!=N;++i)
		println(obf,i);
	}
	std::vector<std::size_t> vec(N);
	{
	fast_io::timer t(u8"input");
	fast_io::ibuf_file ibf(L"iobuf_file.txt");
	for(std::size_t i{};i!=N;++i)
		scan(ibf,vec[i]);
	}
}
